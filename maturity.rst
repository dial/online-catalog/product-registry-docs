Product Maturity Rubrics
========================

.. meta::
   :description lang=en:

There are many different metrics that can be used to evaluate a product and various sources
of information about a metrics such as  sustainability, scalability, sustainability, as well
as adherence to the `Digital Principles`_ 

.. _Digital Principles: https://digitalprinciples.org

The Digital Impact Exchange has maturity data for selected products based on data aggregated
from GitHub repositories. These automated metrics can provide data about how many contributors a 
project have, how often they update the codebase, and whether they have a valid LICENSE. In addition, 
The Exchange aggregates data from Digital Square for their list of Global Goods. 
The maturity scores are not an endorsement of any product, but a reflection of how other organizations
have assessed these tools.

For more information about the metrics that we use, please visit the `Product Evaluation Indicators`_
page in our Wiki. 

.. _Product Evaluation Indicators: https://solutions-catalog.atlassian.net/wiki/spaces/SOLUTIONS/pages/226983937/Product+Evaluation+Indicators

If there is maturity information available about a specfic product, it will be shown on that
product's page. Maturity information is broken down by categories, with individual indicators 
for each category. These categories can be weighted and all of the indicator scores are combined
with the weightings to create an overall maturity score for a product. 


Creating a Maturity Rubric
==========================

DIAL has created a maturity rubric based on data collected from GitHub (where available) as
well as Digital Square. 

Administrative users can create or modify this rubric. Within the rubric, an admin can create/update/remove
categories. For each category, indicators can be added and removed. The weightings for each indicator
in a category can be set (must total 100%). And the weightings for each category in a rubric can be 
set (must total 100%).
