Administrative Functions
========================

.. meta::
   :description lang=en:

A user who has administrator rights will see an 'Admin Data' menu at the top of their screen
after logging in. This menu gives an administrative user the ability to add/remove/update the 
following data: 

 * Countries
 * Projects
 * Sectors
 * Tags
 * Users
 * Candidate Roles
 * Canididate Organizations and Products (submissions to be considered for addition to the Exchange)
 * Maturity Rubrics

Users and Roles
---------------

Administrative users can view the list of user accounts in the Exchange. For any existing user, 
the administrator can edit the user to assign or remove roles to that account. Each user may 
have multiple roles assigned to them.

By default, a user is created with the role 'Basic User'. This means that the user can view information
in the Exchange, can save favorites and searches, and can leave comments.

The following lists additional roles that can be given to a user:

 * Product Owner - a user that can update information about a specific Product
 * Organization Owner - a user that can update information about a specific Organization
 * Content Writer - a user that can modify information about use cases, building blocks, products, organizations, and products - as well as the mappings between them
 * Content Editor - all of the permissions of a content writer, but can also create and delete these objects
 * ICT4SDG User - a user that can modify SDG, Use Case, Workflow, and Building Block information
 * MNI User - a user that can modify information about Mobile Network Aggregators
 * Principle User - a user that can modify information about organizations that are Digital Principles Endorsers
 * Admin User - has full access to all data in the Exchange

An administrator can also delete a user account or create a new user account. Note that new user 
accounts need to be approved by an admin before they can be used. 


Settings
--------

The settings page is used to define values that are used by the Exchange to determine what
information to display. The settings that have been defined include:

 * Default COVID-19 Tag - The Exchange can show products that are designed to be used in COVID-19 response. Products must be tagged with the tag that is defined by this setting to be included in this list.
 * Default Map Center Position - If set to 'country', all map views will center to the current country where the user is located. If set to 'world', it will show the world map
 * Default Maturity Rubric Slug - This defines the slug of the maturity rubric that is used for product evaluations
 * Default Organization - This is the slug of the organization that is the owner of the Exchange.
 * Default Sector List - The name of the list of sectors that will be used in the Exchange

To edit or add new settings, visit the settings page from the menu. 



Maturity Rubrics
----------------

More information on use and configuration of Maturity Rubrics can be found at :doc:`this page <maturity>`


Other Admin Views
-----------------

The remaining views (countries, sectors, tags, etc) can be used to make additions or changes to
these objects. Each of these has a similar layout - when selected, the admin user will see a list of
the objects of that type that has been created in the Exchange. For each object, they can edit or delete
the object. There is also a button at the top of the screen that allows the user to create a new item
of that type. 
