Using the filters
=================

.. meta::
   :description lang=en:

The Exchange offers powerful filtering capabilities that allow the user to see how
SDGs, Use Cases, Workflows, Building Blocks and Products are connected. The filter also
allows users to 

The filter can be opened directly underneath the navigation bar:

.. image:: img/topnav.png
   :width: 800

Click on the 'Filter by' label or arrow to expand the filter section

.. image:: img/filter.png
   :width: 800

For any object (Product, Organization, Building Block, etc) there will be a unique set of 
filters. Some of the filters are aligned with the SDG Digital Investment Framework (SDG, Use Case,
Workflow, or Building Block). Other filters are oriented around sectors, countries, organizations
or tags.

When filters are selected by the user, any items that do not meet the criteria will not be shown. The
top navigation bar will show the number of items that match the selected criteria: 

.. image:: img/filter-counter.png
   :width: 600

At the bottom of the filter section, the user will also see a list of all of the filters that have been 
set. The user can remove filters here.

