Favoriting Features
===================

.. meta::
   :description lang=en:


Users who create an account are able to save products, projects, and use cases that are relevant
for their work or that they may want to come back and revisit. On a list of products, projects, or
use cases, when the user hovers over a card a '+' icon will be visible. By clicking on this icon,
the item will be added to the user's list of favorites.

To view items that have been flagged as favorites, the user can navigate to the 'Profile Settings' 
section of the Exchange. This option is found in the top menu after the user logs in. 

A user can also save a search that they have performed. If a user has selected one or more 
:doc:`filters <filter>`, such as a sector or building block, there will be a link at the top of
the search results allowing them to save this search. This saved search can be found in the 'Profile
Settings' page as well. Selecting the saved search will return the user to the results of that search.
