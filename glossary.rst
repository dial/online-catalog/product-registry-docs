Glossary and Definitions
========================

.. meta::
   :description lang=en:

.. glossary::

    COTS
      Commercial off-the-shelf software

    Digital services
      A broad term referring to the electronic delivery of data or functionality. 

    Digital technologies
      Platforms, processes, and a range of technologies that underpin modern information and communications technologies (ICT), including the internet and mobile phone platforms, as well as advanced data infrastructure and analytical approaches.
    
    ICT Building Blocks
      Reusable software components that enable Workflows and Use Cases across multiple sectors. 

    Open Source
      Software developed by informal collaborative networks of programmers and are usually free. Anyone is freely licensed to use, copy, study, and change the software in any way, and the source code is openly shared so that people are encouraged to voluntarily improve the design of the software. 
        * Free and Open Source (FOSS) – refers to user’s freedom to copy and reuse the software 
        * Free/Libre Open Source software (FLOSS) - Emphasizes the value of ‘libre’, i.e., few or no restrictions. 

    Platform
      A platform is a group of technologies that are used as a base upon which other technologies can be built or applications and services run. For example, the internet is a platform that enables web applications and services.

    SDG Digital Investment Framework (ICT4SDG Framework)
      A framework consisting of four interrelated layers which can be used to help governments and their partners to take a whole-of-government approach to invest in shared digital infrastructure to strengthen SDG programming across sectors.

    SDG Targets
      Each sustainable development goal has a list of targets which are measured with indicators so that governments systematically align their development goals.  

    Sustainable Development Goals (SDGs)
      The SDGs comprise 17 goals and 169 targets representing global priorities for investment in order to achieve sustainable development. The SDGs were set in 2015 by the United Nations General Assembly and intended to be achieved by the year 2030. 

    Use Cases
      Define the steps necessary to achieve a business objective contributing to one or more SDG Targets.  

    Whole-of-government
      An approach where public service agencies (i.e. governments) work across portfolio boundaries to achieve a shared goal and use an integrated government response to particular issues. 

    Whole-of-government approach (WGA)
      Refers to a cross-sectoral and cross-organizational consideration of individuals’ needs with reference to delivering digital services in a more integrated and coordinated manner.  

    Whole of society
      “Acknowledge the contribution of and important role played by all relevant stakeholders, including individuals, families and communities, intergovernmental organizations and religious institutions, civil society, academia, the media, voluntary associations and, where and as appropriate, the private sector and industry, in support of national efforts for noncommunicable disease prevention and control, and recognize the need to further support the strengthening of coordination among these stakeholders in order to improve the effectiveness of these efforts;” (Source: 2011 Political Declaration, (37))

    Workflows
      Are generic business processes, such as ‘client communication’ or ‘procurement’, that support the delivery of a Use Case. 