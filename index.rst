.. Digital Impact Exchange documentation master file, created by
   sphinx-quickstart on Fri Jan 10 14:24:14 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

The Digital Impact Exchange
===========================

The Digital Impact Exchange has been developed by the Digital Impact Alliance (DIAL) 
to be a resource for T4D practitioners to discover 
and evaluate digital public goods that are oriented toward achieving the SDGs. 
The Exchange is built to support the `SDG Digital Investment Framework`_ developed
by DIAL and ITU.

The Exchange aggregates data from a wide range of sources and is regularly updated to 
provide a comprehensive view of technologies that have been developed to support the 
Sustainable Development Goals.

.. _SDG Digital Investment Framework: https://dial.global/research/sdg-digital-investment-framework/

.. toctree::
   :maxdepth: 2
   :caption: Contents:


How to use the Exchange
-----------------------

The Digital Impact Exchange is designed to be flexible and give users multiple starting points
for discovering and evaluating digital platforms. The Exchange has a powerful search and 
filtering system that allows users to clearly see and understand how Sustainable Development 
Goals, Use Cases, Workflows, Building Blocks and Products connect. These connections are all
a part of the SDG Framework.

In addition, the Exchange also provides data on where Products have been deployed (:doc:`Projects <project>`)
and by whom (:doc:`Organizations <organization>`). 

There are two ways to navigate into the Exchange. On the landing page, you will see a section
called 'Getting Started'. You can click on :doc:`Use Cases <use_case>`, 
:doc:`Building Blocks <building_block>`, or :doc:`Products <product>` to navigate to those sections 
within the Exchange:

.. image:: img/landing1.png
   :width: 800

You can also use the top navigation bar to navigate to the different sections of the Exchange. The 
Exchange is made up of three main sections: Catalog, Marketplace, and Resources:

.. image:: img/topnav.png
   :width: 800

Within each section, the Exchange has powerful search and :doc:`filtering <filter>` capabilities. Type 
any text into the search bar to find items that are related to that text. Click on the 'Filter by' label 
or arrow to expand the filtering functions. The filters allow the user to select a specific sector, 
country, use case, SDG, or building block and see the items that match that filtering criteria. 
For example, if a user selects a specific SDG, the Exchange will show the use cases, workflows, building 
blocks, and products that are oriented toward addressing that SDG. See the :doc:`filtering <filter>` 
section for more information.

Finally, the Exchange has several different :doc:`map views <map_view>` that provide information on
where products have been deployed, where organizations work, and what mobile network services are available
in different countries.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: ICT4SDG Framework

   framework
   sdg
   use_case
   workflow
   building_block
   product

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Implementations

   organization
   project
   map_view

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: User Features

   filter
   tagging
   export
   comment
   admin

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Glossary/Definitions

   glossary

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Install/Configure

   install

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: API Documentation

   api