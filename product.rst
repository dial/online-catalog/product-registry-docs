Products
========

.. meta::
   :description lang=en:

This catalog contains an extensive list of digital software tools and datasets that are designed to 
support the SDGs. These products are primarily open-source platforms, but the catalog can be configured
to support proprietary and commercial solutions as well. The list of products in the catalog are sourced 
from several organizations, including DIAL's Open source center, Digital Square, and the Digital Public
Goods Alliance. 

Each card in the products list provides information on the SDGs that the product is designed to support,
any building block functionalities that the product may provide, as well as information on how the product
is licensed. 

At the top of the card, there may be several icons. The check mark icon indicates that the product has 
been endorsed by a vetting organization (such as the Digital Public Goods Alliance). A Digital Principles
icon indicates that the product is maintained by a Digital Principles endorser. And a COVID icon indicates
that the product can be used to support COVID-19 response

Clicking on the card will load a page with detailed information about the product. Each product page 
provides a description of the product, a link to the source code repository, information about organizations
that fund or maintain the product, which SDGs the product is designed to address, any building blocks that 
this product may meet the criteria for. It will also show any sectors that the product supports as well as
information about where the product has been deployed and by whom. If applicable, the catalog will show any 
other products that this platform interoperates with, and in some cases we have maturity information sourced
from DIAL and Digital Square.

Product mappings to Building Blocks or SDGs are flagged either as 'Beta' or 'Validated'. 
A 'Beta' mapping is one that is done through a light overview and vetting of that product. 
A 'Validated' mapping is one that has been done by a group of experts and a more rigorous 
process. These processes are being developed. 

Visit the :doc:`Filters page <filter>` to see how the filters can be used to create
powerful searches that connect SDGs to Use Cases, Workflows, Building Blocks, and 
Products.

A user who is affiliated with a specific product (product owner or maintainer) may sign up to act as
a :doc:`Product Owner <product_owner>` and update information about that product.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Becoming a Product Owner

   product_owner

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Submitting a New Product

   candidate

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Product Maturity

   maturity
