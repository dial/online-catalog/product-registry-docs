Installing and Configuring the Exchange
======================================

.. meta::
   :description lang=en:

This Exchange can be deployed by any organization that wishes to curate their own
list of digital public goods. All of the code is publicly available at the following 
repositories:

 * Back-end/API - `Exchange Back End repository`_
 * Front-end/UX - `Exchange Front End repository`_

**Please note that these instructions are for users who have a technical background and are familiar with Rails, Javascript/React/Next.js, Postgres databases, and Docker deployments**

.. _Exchange Back End repository: https://gitlab.com/dial/t4d-product-backlog/product-registry
.. _Exchange Front End repository: https://gitlab.com/dial/osc/eng/t4d-online-catalog/catalog-front

To configure and run an instance of the Exchange, perform the following steps:

 * Clone the back-end and front-end repositories into separate directories on your server or development environment. 
 * Configure environment variables for the application. The application requires several variables to be set in your environment. 
 
 **Back End**

 The following environment variables are required for the Exchange Back end. We have provided a sample shell script that will set the appropriate variables for development, test, or production environments. The variables are as follows:

  * SECRET_KEY_BASE - set a unique secret or password. This is used for user authentication verification
  * DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD - connecting data for the Postgres database. Ensure that these values match the settings in the docker-compose.yml file for the database instance (described below)
  * TEST_DATABASE_NAME, TEST_DATABASE_USER, TEST_DATABASE_PASSWORD - to run the unit or integration tests, these values need to be set and point to a test database instance
  * ESRI_CLIENT_ID, ESRI_CLIENT_SECRET - the application uses `ESRI`_ for geographic data. You can sign up for an account and access token.
  * GOOGLE_TRACKER - insert a tracking id to use Google analytics
  * JENKINS_URL, JENKINS_USER, JENKINS_PASSWORD - in order to use the product launcher functionality, the application must connect to a running Jenkins instance. 
  * MAILGUN_API_KEY, MAILGUN_DOMAIN - the application uses mailgun to send email notifications to users. Use mailgun account data here to enable this functionality
  * CAPTCHA_SITE_KEY, CAPTCHA_SECRET_KEY - the main signup page uses Captcha. Enter your captcha credentials to use this functionality
  * GITHUB_USERNAME, GITHUB_PERSONAL_TOKEN - the appication will automatically read data from any GitHub repositories that are connected to products. In order to do that, enter a Github username and API Token.

 **Front End**

 The following environment variables are required for the Exchange Front End. These should be stored in a file called .env.production

 * NEXT_PUBLIC_GRAPHQL_SERVER - URL for the Back End Server 
 * NEXT_PUBLIC_AUTH_SERVER - URL for authentication (this will be the same as the Back End server)
 * NEXTAUTH_URL - This is the callback endpoint for authentication - this should be the URL for the Exchange Front end
 * SECRET - Unique secret key for Next.js front end 
 * NEXT_PUBLIC_DISCOURSE_PROD_URL - For integrating Discourse forums for products, provide the URL for the Product category in Discourse
 * NEXT_PUBLIC_DISCOURSE_PROD_KEY - For integrating Discourse forums for products, link to Discourse API key
 * NEXT_PUBLIC_DISCOURSE_BB_URL - For integrating Discourse forums for Building Blocks, provide the URL for the BB category in Discourse
 * NEXT_PUBLIC_DISCOURSE_BB_KEY - For integrating Discourse forums for Building Blocks, link to Discourse API key
 * NEXT_PUBLIC_API_URL - Set to the URL for the Exchange Front End
 * NEXT_PUBLIC_RAILS_SERVER - URL for Back End Server
 * NEXT_PUBLIC_GOOGLE_ANALYTIC_ID - Add a GA key for tracking analytics
 * NEXT_PUBLIC_CAPTCHA_SITE_KEY - Add a key to enable Captcha on user sign ups

Running the application

 * Edit the docker-compose.yml file in the Exchange back end to set the database username and password. 
 * Build and run the application using the following docker-compose commands from the root directory of the application:
  
  * docker-compose build
  * docker-compose up -d

 * Alternatively, to run the app outside of docker-compose, you can use the following commands:

 **Back End**

  * rails db:run_if_no_db && rails db:create_db_with_public_data
  * rails db:migrate
  * rails assets:precompile RAILS_ENV=production
  * rails server -e production

 **Front End**

  * yarn install
  * yarn build
  * yarn start

 * Once the application is built and started, it will be available on port 3002. The back end/API will be available on port 3000. Please note that the docker-compose file also uses nginx and certbot to serve the application using SSL/Port 443. Please remove these entries if not needed. 
 
 * The application expects a Redis server to be running. The Redis server is automatically started as part of the docker-compose. If starting the Exchange manually, Redis must be running on your server.

 * A default user account with administrator privileges is created automatically. The credentials are admin@admin-user.org/admin-password

 * The application also allows a default domain name to be set. Only users that have email addresses associated with that domain will be able to be administrators of the site. To set this:
 
  * Create an organization entry for your organization. Note the slug that is created for this organization
  * Navigate to the settings page (<online Exchange URL>/settings)
  * Create a new setting called 'Default Organization'. The value for this setting will be the slug for your organization.
  
.. _ESRI: https://www.esri.com/