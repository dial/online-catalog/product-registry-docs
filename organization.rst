Organizations
=============

.. meta::
   :description lang=en:

The Digital Impact Exchange currently tracks different types of organizations:
 * Endorsers of DIAL's `Principles for Digital Development`_
 * Mobile Network Aggregators/Integrators
 * Owners, funders, and maintainers of digital tools

.. _Principles for Digital Development: https://digitalprinciples.org

Users can see a list of these organizations and can also view the organizations
using the interactive :doc:`map view <map_view>`

When using the organization page, users can use :doc:`filters <filter>` to show 
organizations that are working in a specific geographic location or in a particular
development sector. The filters can also refine the list to show only endorsing organizations
or mobile network integrators. 

A user who is affiliated with a specific organization may sign up to act as an
:doc:`Organization Owner <org_owner>` and update information about that organization.

Endorsers
  Many organizations working around the world have endorsed a set of 9 Principles 
  for Digital Development. For each of these organizations, the Exchange provides
  information on whatr countries they work in, where offices are located, and what Development
  sectors they are focused on. In some cases, these organizations have developed a software
  or technical product and the Exchange provides a link to that product. 


Mobile Network Aggregators/Integrators
  DIAL has developed a list of mobile network integrators (also called aggregators) that provide
  mobile service delivery around the world. The Exchange can help users understand which
  mobile technologies are offered in a specific geography. On the aggregator page, it will show which
  countries an aggregator works in, which mobile network operators they partner with, and the specific
  service offerings that they provide. 

Owners, Maintainers, and funders
  The Exchange tracks organization who are responsible for the development, funding, and maintenance
  of software products. The Exchange provides information on which organizations are connected to 
  which products in these various capacities.


.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Becoming an Organization Owner

   org_owner